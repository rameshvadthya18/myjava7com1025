package com.hertfordshire.fitness;
import java.nio.file.attribute.AclEntry.Builder;
import java.util.Scanner;


public class TimeTable
{

    static String[] availableDates = new String[] { "02/05/2020", "03/05/2020", "09/05/2020", "10/05/2020", "16/05/2020", 
    		"17/05/2020", "23/05/2020", "24/05/2020", "30/05/2020", "31/05/2020", "06/06/2020", "07/06/2020", "13/06/2020", 
    		"14/06/2020", "20/06/2020", "21/06/2020" };
    String[] days= new String[] { "Saturday", "Sunday" };
    String[] timings= new String[] { "9.30 AM", "1.30 PM", "5.30 PM" };
    static double[] cost = new double[] { 100.0, 120.0, 115.0, 120.0, 110.0 };
    static String[][] schedules = new String[16][3];
    static String[] classes = new String[] { "Yoga", "Zumba", "Aquacise", "Box Fit", "Body Blitz" };
    Scanner scanner = new Scanner(System.in);
    
   
    public void buildTimeTable(){
    	int k = 0;
        for(int i=0;i<16;i++) {
            int timeIndex = 0;
            for (int j = 0; j < 3; ++j) {
                final int classIndex = k % 5;
                TimeTable.schedules[i][j] = String.valueOf(timings[timeIndex]) + " - " + TimeTable.classes[classIndex];
                ++k;
                timeIndex++;
            }
        }
    }
    
    TimeTable() {
    	buildTimeTable();
    }
    
    public void showTimeTable() {
        int choice = 0;
        do {
            System.out.println("\nTime Table options:");
            System.out.println("1. View all\n2. Search by date\n3. Search by Class name\n4. Go Back");
            System.out.print("Enter your choice: ");
            choice = scanner.nextInt();
            if(choice == 1) {
                    showAll();
                }
            else if(choice == 2){
                System.out.print("Available dates:");
                for (int i = 0; i < 16; ++i) {
                    if (i % 6 == 0) {
                        System.out.print("\n");
                    }
                    System.out.print(String.valueOf(TimeTable.availableDates[i]) + "\t");
                }
                System.out.print("\n\nEnter the date(DD/MM/YYYY): ");
                 String date = scanner.next();
                 boolean res = searchDate(date);
                if (!res) {
                    System.out.println("Enter the correct availabe date");
                }
            }
            else if(choice == 3){
                    System.out.println("Available Classes are: Yoga, Zumba, Aquacise, Box Fit, Body Blitz");
                    System.out.print("Enter the Class name: ");
                     String className = scanner.next();
                    boolean resClass = searchName(className);
                    if (!resClass) {
                        System.out.println("Enter the correct availabe class name");
                        continue;
                    }

                }
            else if(choice !=4){
                    System.out.println("Enter valid choice");
                }
            } while (choice != 4);
        }
    
    public void showAll() {
        for (int i = 0; i < 16; ++i) {
            final int dayIndex = i % 2;
            System.out.println(String.valueOf(TimeTable.availableDates[i]) + " " + days[dayIndex]);
            for (int j = 0; j < 3; ++j) {
                System.out.println(TimeTable.schedules[i][j]);
            }
            System.out.print("\n");
        }
    }
    
    public boolean searchName(final String className) {
        int flag = 0;
        System.out.println("Schedules for " + className + ":");
        int i=0;
        while(i < 16){
            for (int j = 0; j < 3; ++j) {
                if (TimeTable.schedules[i][j].toLowerCase().contains(className.toLowerCase())) {
                    System.out.println(String.valueOf(TimeTable.availableDates[i]) + "-" + TimeTable.schedules[i][j].substring(0, 7));
                    flag = 1;
                }
            }
            i++;
        }
        return flag != 0;
    }
    
    public boolean searchDate(final String date) {
    	int i=0;
        while(i < 16){
            if (TimeTable.availableDates[i].equals(date)) {
                final int dayIndex = i % 2;
                System.out.println(String.valueOf(TimeTable.availableDates[i]) + " " + days[dayIndex]);
                for (int j = 0; j < 3; ++j) {
                    System.out.println(TimeTable.schedules[i][j]);
                }
                return true;
            }
            i++;
        }
        return false;
    }
    

}
