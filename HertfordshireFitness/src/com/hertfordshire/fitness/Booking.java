package com.hertfordshire.fitness;

import java.util.Scanner;
 

public class Booking
{
    Scanner sc = new Scanner(System.in);
    TimeTable eventSchedule = new TimeTable();
    static String[][] bookingName;
    static int[] bookingCount;
    int index;
    int studNameIndex = 0;
    String[] exercises= new String[] { "Yoga", "Zumba", "Aquacise", "Box Fit", "Body Blitz" };
    
    static {
        Booking.bookingName = new String[5][4];
        Booking.bookingCount = new int[5];
    }

    
    public void bookingAClass() {
        System.out.println("check the Time Table before booking");
        System.out.print("Do you want to check?(Y/N): ");
        final String check = sc.nextLine();
        if (check.equalsIgnoreCase("Y")) {
            eventSchedule.showTimeTable();
        }
        System.out.print("Enter Name: ");
        final String studName = sc.nextLine();
        System.out.print("Name of the class you wish to join: ");
        final String className = sc.nextLine();
        if (checkAvailability(className)) {
            book(studName, className);
        }
        else {
            System.out.println("Class you choose is not available");
        }	
          
    }
    
    public void book(final String studName, final String className) {
        studNameIndex = Booking.bookingCount[index];
        Booking.bookingName[index][studNameIndex] = studName;
        Booking.bookingCount[index] = Booking.bookingCount[index] + 1;
        System.out.println("You have booked for " + className + " class successfully.");
        eventSchedule.searchName(className);
    }
    
    public boolean checkAvailability(final String className) {
        boolean found = false;
        int i=0;
        while(i < 5) {
            if (exercises[i].equalsIgnoreCase(className)) {
                index = i;
                found = true;
            }
            i++;
        }
        if (!found) {
            System.out.println("Enter correct name of the  class");
            return false;
        }
        if (Booking.bookingCount[index] < 4) {
            return true;
        }
        System.out.println("The class you are selected is filled.");
        return false;
    }
}
