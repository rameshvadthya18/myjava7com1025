package com.hertfordshire.fitness;
import java.util.Scanner;

public class BookingApplication
{
	
	static Scanner scanner = new Scanner(System.in);
    public static void main(final String[] args) {
        TimeTable timeTable = new TimeTable();
        ClassesList classesList = new ClassesList();
        Booking booking = new Booking();
        Rate rating = new Rate();
        Summary summary = new Summary();
        int input = 0;
        System.out.println(" *********Hertfordshire Sports Centre ***********");
        do {
            System.out.println("Please make a choice from below options");
            System.out.println("1. Display Exercises offered \n2. Check Available Schedule\n3. Book A Class\n4. Rate Us  \n5. Print summary\n6. Exit");
        	input = scanner.nextInt();
            if(input == 1) {
                classesList.showClasses();
            }else if(input == 2) {
                timeTable.showTimeTable();
            }
            else if(input == 3) {
                booking.bookingAClass();
            }
            else if(input == 4) {
                rating.startRating();
            }
            else if(input == 5){
                summary.displaySummary();
            }
            else if(input == 6){
                System.out.println("Booking section ends");
                break;
            }
            else {
                System.out.println("Select correct option from 1-6 : ");
            }
        } while(true);
    }
}
