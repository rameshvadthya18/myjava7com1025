package com.hertfordshire.fitness;
import java.util.ArrayList;
import java.util.Scanner;
 

public class Rate
{
    Scanner scanner = new Scanner(System.in);
    String[] classes = new String[] {"Yoga", "Zumba", "Aquacise", "Box Fit", "Body Blitz"};
    ArrayList<Integer> rows = new ArrayList<Integer>();
    ArrayList<Integer> cols = new ArrayList<Integer>();
    ArrayList<String> classesAttended = new ArrayList<String>();
    String className;
    static int[][] rating =new int[5][4];
    Booking booking = new Booking();
    String userName;
    String[][] reviews = new String[5][4];

    
    public void getData() {
    	int i=0;
    	while(i<classesAttended.size()) {
    		System.out.print("Give your review about " + classesAttended.get(i) + ": ");
            reviews[rows.get(i)][cols.get(i)] = scanner.nextLine();
            scanner.nextLine();
            System.out.print("Give the ratings for " + classesAttended.get(i) + ": ");
            Rate.rating[rows.get(i)][cols.get(i)] = scanner.nextInt();
            i++;
    	}
        
        System.out.println("Your rating(s) and review(s) stored successfully");
      
    }
    

    public boolean checkForClass(String studName) {
        classesAttended.clear();
        rows.clear();
        cols.clear();
        boolean registered = false;
        int i=0;
        while(i < 5) {
            for (int j = 0; j < Booking.bookingCount[i]; ++j) {
                String tmp = Booking.bookingName[i][j];
                if (tmp.equalsIgnoreCase(studName)) {
                    classesAttended.add(classes[i]);
                    rows.add(i);
                    cols.add(j);
                    registered = true;
                    break;
                }
            }
            i++;
        }
        if (! registered) {
            System.out.println("You must book a class before to review"); 
        } else {
        	System.out.println("You are registered for the following classes");
            i=0;
           while( i < classesAttended.size()) {
               System.out.println(String.valueOf(classesAttended.get(i)) + " ");
               i++;
            }
        }
        
        return registered;
    }
    public void startRating() {
        classesAttended.clear();
        rows.clear();
        cols.clear();
        System.out.print("Enter  Name: "); //here 
        if(checkForClass(userName = scanner.next())) {
        	System.out.println("1: Very Good, 2: Good, 3: Ok, 4: Bad, 5: Very Bad");
            getData();
        }
        
    }
    
}
