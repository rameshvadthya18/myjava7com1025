package com.hertfordshire.fitness;


public class ClassesList
{
    public void showClasses() {
    	
    	System.out.println("Class code\tClass Name\tCost\n"
        		+ "HF01\t\tYoga\t\t100\n"
        		+ "HF02\t\tBody Blitz\t110\n"
        		+ "HF03\t\tBox Fit\t\t120\n"
        		+ "HF04\t\tAquacise\t115\n"
        		+ "HF05\t\tZumba\t\t120\n");

    }
}
