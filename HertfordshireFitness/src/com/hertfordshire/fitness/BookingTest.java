package com.hertfordshire.fitness;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BookingTest {
	

	Booking bookingTest=new Booking();
	Rate rateTest=new Rate();
	@Test
	void checkAvailabilityTest() {
		boolean result=bookingTest.checkAvailability("Yoga");
		assertEquals(true, result);
		
		result=bookingTest.checkAvailability("Zumba");
		assertEquals(true, result);
	}
	
	@Test
	void findClassesTest() {
		boolean result=rateTest.checkForClass("Ramesh Vadthya");
		assertEquals(false, result);
	}

	
	
}
