import java.util.Scanner;

// 
// Decompiled by Procyon v0.5.36
// 

public class Booking
{
    Scanner sc = new Scanner(System.in);
    TimeTable timeTable = new TimeTable();
    static String[][] bookedName;
    static int[] bookedCount;
    int classIndex;
    int studNameIndex = 0;
    String[] classes= new String[] { "Yoga", "Zumba", "Aquacise", "Box Fit", "Body Blitz" };
    
    static {
        Booking.bookedName = new String[5][4];
        Booking.bookedCount = new int[5];
    }

    
    public void bookingClass() {
        System.out.println("Before booking check the Time Table");
        System.out.print("Do you want to check?(Y/N): ");
        final String check = sc.nextLine();
        if (check.equalsIgnoreCase("Y")) {
            timeTable.showTimeTable();
        }
        System.out.print("Enter your Name: ");
        final String studName = sc.nextLine();
        System.out.print("Enter the class you wish to join: ");
        final String className = sc.nextLine();
        if (checkAvailability(className)) {
            book(studName, className);
        }
        else {
            System.out.println("Booking failed.");
        }
    }
    
    public void book(final String studName, final String className) {
        studNameIndex = Booking.bookedCount[classIndex];
        Booking.bookedName[classIndex][studNameIndex] = studName;
        int[] bookedCount = Booking.bookedCount;
        int classIndex = this.classIndex;
        ++bookedCount[classIndex];
        System.out.println("You are booked for " + className + " class successfully.");
        timeTable.searchName(className);
    }
    
    public boolean checkAvailability(final String className) {
        int flag = 0;
        int i=0;
        while(i < 0) {
            if (classes[i].equalsIgnoreCase(className)) {
                classIndex = i;
                flag = 1;
            }
            i++;
        }
        if (flag == 0) {
            System.out.println("Enter correct class name");
            return false;
        }
        if (Booking.bookedCount[classIndex] < 4) {
            return true;
        }
        System.out.println("The class you are selected filled.");
        return false;
    }
}
