import java.util.Scanner;

public class BookingApplication
{
	
	static Scanner scanner = new Scanner(System.in);
    public static void main(final String[] args) {
        TimeTable timeTable = new TimeTable();
        ClassesList classesList = new ClassesList();
        Booking booking = new Booking();
        Rate rating = new Rate();
        Summary summary = new Summary();
        int input = 0;
        System.out.println(" *********University Sports Centre ***********");
        System.out.println("please Select the  options");
        System.out.println("1. List of exercise classes Available \n2. Time Table\n3. Book Class\n4. Rate Us  \n5. Print summary\n6. Exit");
        for(;;) {
        	System.out.print("Select your option: ");
        	input = scanner.nextInt();
            if(input == 1) {
                classesList.showClasses();
            }else if(input == 2) {
                timeTable.showTimeTable();
            }
            else if(input == 3) {
                booking.bookingClass();
            }
            else if(input == 4) {
                rating.startRating();
            }
            else if(input == 5){
                summary.displaySummary();
            }
            else if(input == 6){
                System.out.println("Booking section ends");
                break;
            }
            else {
                System.out.println("Select correct option from 1-6 : ");
            }
        }
    }
}
