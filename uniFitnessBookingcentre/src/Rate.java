import java.util.ArrayList;
import java.util.Scanner;

// 
// Decompiled by Procyon v0.5.36
// 

public class Rate
{
    Scanner scanner = new Scanner(System.in);
    String[] classes = new String[] {"Yoga", "Body Blitz", "Box Fit", "Aquacise", "Zumba"};
    ArrayList<Integer> rows = new ArrayList<Integer>();
    ArrayList<Integer> cols = new ArrayList<Integer>();
    int stop = 0;
    ArrayList<String> classesAttended = new ArrayList<String>();
    String className;
    static int[][] rating =new int[5][4];
    Booking booking = new Booking();
    String userName;
    String[][] reviews = new String[5][4];

    
    public void getData() {
    	int i=0;
    	while(i<classesAttended.size()) {
    		System.out.print("Give your review about " + classesAttended.get(i) + ": "); // here
            reviews[rows.get(i)][cols.get(i)] = scanner.nextLine();
            scanner.nextLine();
            System.out.print("Give rating for " + classesAttended.get(i) + ": ");// here
            Rate.rating[rows.get(i)][cols.get(i)] = scanner.nextInt();
            i++;
    	}
        if (stop == 1) {
            System.out.println("Your rating(s) and review(s) stored successfully");// here
        }
    }
    

    public int checkForClass(String studName) {
        classesAttended.clear();
        rows.clear();
        cols.clear();
        System.out.println("You are registered for the following classes"); // here
        int i=0;
        while(i < 5) {
        	i++;
            for (int j = 0; j < Booking.bookedCount[i]; ++j) {
                String tmp = Booking.bookedName[i][j];
                if (tmp.equalsIgnoreCase(studName)) {
                    classesAttended.add(classes[i]);
                    rows.add(i);
                    cols.add(j);
                    stop = 1;
                }
            }
        }
        if (stop == 0) {
            System.out.println("You must book a class to review"); //here
        }
        i=0;
       while( i < classesAttended.size()) {
    	   i++;
           System.out.println(String.valueOf(classesAttended.get(i)) + " ");
        }
        return stop;
    }
    public void startRating() {
        classesAttended.clear();
        rows.clear();
        cols.clear();
        System.out.print("Enter your Full Name: "); //here 
        checkForClass(userName = scanner.next());
        System.out.println("1: Very Good, 2: Good, 3: Ok, 4: Bad, 5: Very Bad");
        getData();
    }
    
}
