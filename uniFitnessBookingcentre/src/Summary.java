
import java.util.Scanner;

// 
// Decompiled by Procyon v0.5.36
// 

public class Summary
{

    Scanner scanner = new Scanner(System.in);
    String date;
    int[] index = new int[3];
    String[] classes = new String[3];
    double[] cost = new double[5];
    public void displaySummary() {
        
        int input = 0;
        while (input != 3) {
            System.out.println("Select a choice"); // here
            System.out.println("1. Daywise report with average rating\n2. Class with highest income\n3. Main menu"); // here
            System.out.print("Enter your choice: ");
            input = scanner.nextInt();
            scanner.nextLine();
            if(input == 1) {
                    System.out.print("Available dates:");
                    int i=0;
                    while(i<16) {
                        if (i % 6 == 0) {
                            System.out.print("\n");
                        }
                        System.out.print(String.valueOf(TimeTable.Availabledates[i]) + ", ");
                        i++;
                    }
                    System.out.print("\n\nEnter the date (DD/MM/YYYY): ");
                    daywiseReport(date = scanner.nextLine());
                }
            else if(input ==2)
            	highestIncome();
            else 
            	System.out.println("Enter valid choice");
                
            }
    }
    
    public void highestIncome() {
        int index = 0;
        int i=0;
        int j=0;
        while(i < 5) {
            this.cost[i] = Booking.bookedCount[i] * TimeTable.cost[i];
            i++;
        }
        double max = this.cost[0];
       while(j < 5) {
            if (this.cost[j] > max) {
                max = this.cost[j];
            }
            j++;
        }
        while(j < 5) {
            if (this.cost[j] == max) {
                index = j;
            }
            j++;
        }
        System.out.println("Highest income is " + max + " from " + TimeTable.classes[index] + " class");
    }
    
    public boolean daywiseReport(final String date) {
        for (int i = 0; i < 16; ++i) {
            if (date.equals(TimeTable.Availabledates[i])) {
                for (int j = 0; j < 3; ++j) {
                    this.classes[j] = TimeTable.schedules[i][j];
                    System.out.println(this.classes[j]);
                    if (this.classes[j].contains("Yoga")) {
                        System.out.println("Booked count for this course is: " + Booking.bookedCount[0]);
                        final double avgRating = this.averageRating(0);
                        System.out.println("The average rating for this class is: " + avgRating);
                        return true;
                    }
                    else if (this.classes[j].contains("Zumba")) {
                        System.out.println("Booked count for this course is: " + Booking.bookedCount[1]);
                        this.averageRating(1);
                        return true;
                    }
                    else if (this.classes[j].contains("Aquacise")) {
                        System.out.println("Booked count for this course is: " + Booking.bookedCount[2]);
                        this.averageRating(2);
                        return true;
                    }
                    else if (this.classes[j].contains("Box Fit")) {
                        System.out.println("Booked count for this course is: " + Booking.bookedCount[3]);
                        this.averageRating(3);
                        return true;
                    }
                    else if (this.classes[j].contains("Body Blitz")) {
                        System.out.println("Booked count for this course is: " + Booking.bookedCount[4]);
                        this.averageRating(4);
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public double averageRating(final int index) {
        int totRating = 0;
        for (int i = 0; i < Booking.bookedCount[index]; ++i) {
            totRating += Rate.rating[index][i];
        }
        return totRating / Booking.bookedCount[index];
    }
}
